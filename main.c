/*
 * Arel Jann Clemente | 991436441 | Sorting Text File By Length Using C
 * Alex  Babanski - Data Structures and Algorithms in C
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned int MAX_SIZE = 100;
typedef unsigned int uint;

// This function will be used to swap "pointers".
void swap(char**, char**);

// Bubble sort function here.
void bubbleSort(char**, uint);

// Read quotes from quotes.txt file file and add them to array. Adjust the size as well!
// Note: size should reflect the number of quotes in the array/quotes.txt file!
void read_in(char**, uint*);

// Print the quotes using array of pointers.
void print_out(char**, uint);

// Save the sorted quotes in the output.txt file
void write_out(char**, uint);

// Free memory!
void free_memory(char**, uint);

int main() {
    // Create array of pointers. Each pointer should point to heap memory where
    // each quote is kept. I.e. arr[0] points to quote N1 saved on the heap.
    char *arr[MAX_SIZE];

    // Number of quotes in the file quotes.txt. Must be adjusted when the file is read!
    uint size = MAX_SIZE;

    read_in(arr, &size);

    printf("--- Input:\n");
    print_out(arr, size);

    bubbleSort(arr, size);

    printf("--- Output:\n");
    print_out(arr, size);
    write_out(arr, size);

    free_memory(arr, size);

    return (0);
}
// This function can be found right here: https://bit.ly/2JBpVNv
// This function swaps the addresses of the 2 given pointers.
void swap(char **ptr1, char **ptr2) {
    char *temp = *ptr1;
    *ptr1 = *ptr2;
    *ptr2 = temp;
}
// This function can be found right here: https://bit.ly/2JBpVNv
// It is modifed to compare 2 strings at once and swap them.
void bubbleSort(char** arr, uint size) {
    register uint i, j;
    for (i = 0; i < size - 1; i++) {
        for (j = 0; j < size - i - 1; j++) {
            if (strlen(arr[j]) > strlen(arr[j + 1])) {          // This compares the lengths of 2 strings.
                swap(&arr[j], &arr[j + 1]);                     // If string 1 > string 2, then swap addresses.
            } else if (strlen(arr[j]) == strlen(arr[j + 1])) {  // This compares the lengths of 2 strings; if they're equal in length, call strcoll(). 
                if (strcoll(arr[j], arr[j + 1]) > 0) {          // strcoll() "sorts" these strings alphabetically... let it do its magic.
                    swap(&arr[j], &arr[j + 1]);                 // If string 1 is alphabetically first, then swap addresses with string 2.
                }
            }
        }
    }
}
// This function will read a text file and dynamically allocate memory for the number of lines it has.
// The strings will be placed in an array (this function still reads empty lines... needs a simple fix I can't find).
void read_in(char** arr, uint* size) {
    // Open file and check if it can be opened. If not, program will terminate.
    FILE *fp;
    if ((fp = fopen("quotes.txt", "r")) == NULL) {
        perror("Cannot open file; fopen()\n");
        exit(1);
    }
    // This code can be found here: https://bit.ly/2y81liA
    uint lines = 0;
    char ch;
    while ((ch = fgetc(fp)) != EOF) {
        if (ch == '\n') {
            lines++;
        }
        if ((ch = fgetc(fp)) == '\n') {
            fseek(fp, -1, 1);
            lines--;
        }
    }
    // Change the value of size to the lines of the text.
    *size = lines;
    // Allocate memory for each array to hold quotes.
    register uint i;
    for (i = 0; i < lines; i++) {
        arr[i] = (char*) calloc(200, 200 * sizeof (char));
        if (arr[i] == NULL) {
            printf("Cannot allocate memory for line %u.", i + 1);
            exit(1);
        }
    }
    // Rewind file pointer to beginning of file.
    rewind(fp);
    // Place quotes from text file to an array.
    i = 0;
    char buffer[200]; // This buffer will read the text.
    while (!feof(fp)) {
        if (fgets(buffer, sizeof (buffer), fp) != NULL) {
            buffer[strcspn(buffer, "\n")] = '\0';           // Remove \n character from the buffer.   
            buffer[strcspn(buffer, "\r")] = '\0';           // Remove \r character from the buffer.    
            if (strlen(buffer) > 0) {
                memcpy(arr[i], buffer, strlen(buffer) + 1); // Copy quote stored from buffer to an array element.
                i++;                
            }
        }
    }
}
// This function prints out quotes stored in the array of strings.
void print_out(char** arr, uint size) {
    register uint i;
    for (i = 0; i < size; i++) {
        printf("%s\n", arr[i]);
    }
}
// This function writes the quotes stored in the array of strings to a file.
void write_out(char** arr, uint size) {
    FILE *fp;
    if ((fp = fopen("output.txt", "w")) == NULL) {
        perror("Cannot open file; fopen()\n");
        exit(1);
    }
    register uint i;
    for (i = 0; i < size; i++) {
        fprintf(fp, "%s\n", arr[i]);
    }
    fclose(fp);
}
// This function frees the memory used by the dynamically allocated array of strings to store the quotes.
void free_memory(char** arr, uint size) {
    register uint i;
    for (i = 0; i < size; i++) {
        free(arr[i]);
    }
}